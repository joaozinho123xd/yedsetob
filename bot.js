const tmi = require('tmi.js');
const request = require('request');
//credenciais: un/pwd de quem vai "executar" os comandos. chn é o canal onde esse bot atuará.



const un = "tesdbota";
const pwd = "oauth:jnzcdk4y6jaa0nujmwotqxtnz2rsts"
const chn = "tesdey";


//canal = de onde irá pegar a lista de usuários (exceto para propósitos de debug, deverá ser o mesmo canal de atuação do bot).
//moderador = é quem é capaz de enviar comandos
//nota: é bom que o moderador e o bot sejam contas diferentes para evitar atrasos na entrada de comandos.
const canal = "tesdey";
const moderador = "tesdey";
const opts = {
  connection: {
        reconnect: true
    },
  identity: {
    username: un,
    password: pwd
  },
  channels: [
    chn
  ]
};
const client = new tmi.client(opts);

//ID das premiações
const unTo = "100eb64e-289b-4146-8022-9a9e0e8608ed";
const timeOut = "b8f63490-8f66-436a-b81f-1fa278a531ff";

//array de moderadores pro "reflect" do TO
var moderadores = ['tesdey'];

var isBattleRoyaleUp = false;
var inSafeZone = [];

initialize();

//Handlers
function onMessageHandler (target, context, msg, self) {
  if (self) { return; }

  const entry = msg.trim();
  const commandName = entry.replace('@', '');
  const rewardId = context["custom-reward-id"];
console.log(commandName);
  if(rewardId == unTo) untimeout(target, commandName);

  if(rewardId == timeOut) {
    client.mods("tesdey");
    if(isTryingToBanMod(commandName.toLowerCase())){
      timeout(target, context.username);
    } else{
      timeout(target, commandName);
    }
  }

  if(isBattleRoyaleUp && !inSafeZone.includes(context.username)){//adiciona qualquer um que enviar uma mensagem à um array inSafeZone. Somente quando estiver ocorrendo o battle royale
    inSafeZone.push(context.username);
  }
  if(context.username == moderador){

    //comandos de debug
    if(commandName === '!isz'){
      console.log("In safe zone: "+inSafeZone);
    }
    if(commandName === '!gc'){ //receber lista de chatters
      getchatters();
    }
    //comandos
    if (commandName === '!campers'){
      killCampers(target, inSafeZone);
    }
    if (commandName === '!iniciar') { //inicia o battle royale
        isBattleRoyaleUp = true;
        battleRoyale(target);
    }
    if (commandName === '!limpar'){ //limpa lista de campers
      inSafeZone = [];
    }
  }
}

function onModsHandler (channel, mods){
  moderadores = mods;
  moderadores.push("tesdey");
}

function killCampers(target, inSafeZone){
  request("https://tmi.twitch.tv/group/user/tesdey/chatters", { json: true }, (err, res, body) => {
    if (err) { return console.log(err); }

    var rosesLogadasTotais = Object.keys(body.chatters.viewers).length;
    var i = 0;

    for(i = 0; i < rosesLogadasTotais; i++){
      var atual = body.chatters.viewers[i];
      if(inSafeZone.includes(atual.toString())){
        continue;
      } else{
        timeout(target, atual);
      }
    }
  });
}//function

//vai banir aleatoriamente <sacrificio> pessoas do chat: onde sacrifício, é o número total de pessoas dividido por fator de porcentagem
function battleRoyale(target){
  request("https://tmi.twitch.tv/group/user/"+canal+"/chatters", { json: true }, (err, res, body) => {
    if (err) { return console.log(err); }

    var rosesLogadasTotais = Object.keys(body.chatters.viewers).length;
    var sacrificio = Math.floor(rosesLogadasTotais/1.1);

    var i = 0;
    console.log("INDO BANIR "+sacrificio);
    for(i = 0; i < sacrificio; i++){
      var sorteado = sortear(body, target, rosesLogadasTotais);
      timeout(target, sorteado);
    }
  });
}

//helpers de TimeOut
function sortear(body, target, rosesLogadasTotais){
  var posicaoEscolhido = getRandomInt(0, rosesLogadasTotais);
  var escolhido = body.chatters.viewers[posicaoEscolhido];
  return escolhido;
}

function timeout(target, escolhido){
  client.say(target, '/timeout '+escolhido);
}

function untimeout(target, escolhido){
  client.say(target, '/untimeout '+escolhido);
}

function isTryingToBanMod(commandName){
  if(moderadores.includes(commandName)) return true;
  else return false;
}


//funções utilitárias
function getRandomInt(min, max) {
  min = Math.ceil(min);
  max = Math.floor(max);
  return Math.floor(Math.random() * (max - min)) + min;
}

function initialize(){
  client.on('message', onMessageHandler);
  client.on('connected', onConnectedHandler);
  client.on('mods', onModsHandler);
  client.connect();
}


//funções de debug
function getchatters(){
  request("https://tmi.twitch.tv/group/user/tesdey/chatters", { json: true }, (err, res, body) => {
    if (err) { return console.log(err); }
    console.log(body.chatters.viewers);
  });
}

function onConnectedHandler (addr, port) {
  console.log(`* Iniciado com sucesso em: ${addr}:${port}`);
}
